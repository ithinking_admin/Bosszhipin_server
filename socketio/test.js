module.exports = function (server) {
    // 得到 IO 对象
    const io = require('socket.io')(server);
    // 监视连接 ( 当有一个客户连接上时回调 )
    io.on('connection', function (socket) {
        console.log('有一个soketio连接上了----> ' + socket.id + new Date());

        socket.on('send', function (obj) {
            console.log('服务器接收到 '+obj.from+' 的消息', obj.content,`目标： ${obj.to}`);
            let target = obj.to.toString();
            io.emit(target, obj);
            console.log(`服务器向 ${obj.to} 发送 ${obj.content} `)
        })

    });

    io.on('disconnect', function (socket) {
        console.log('有一个soketi断开了----> ' + socket.id + new Date());

    })
};